# img-learn

## Pré-requisitos
- Nodejs
- Git

## Instalação
Para clonar e rodar o código, são necessários os seguintes comandos:

```sh
npm install -g @ionic/cli cordova-res
git clone https://gitlab.com/paranauerjni/img-learn.git img-learn
cd img-learn
npm install
ionic serve
```

Explicação das linhas
1 - Instala o ionic
2 - Clona o repositório
3 - Abre a pasta onde está o img-learn
4 - Instala as dependências
5 - Roda o código
