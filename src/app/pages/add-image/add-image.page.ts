import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-add-image',
  templateUrl: './add-image.page.html',
  styleUrls: ['./add-image.page.scss'],
})
export class AddImagePage implements OnInit {

  constructor(private navController: NavController, private toastController: ToastController) { }

  ngOnInit() {
  }

  cancelOption(){
    this.navController.navigateRoot("main");
  }

  addOption(){
    this.presentToast();
    // this.navController.navigateRoot("main");
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Imagem adicionada',
      duration: 2000
    });
    toast.present();
  }

}
