import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import Chart from 'chart.js/auto'

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
  @ViewChild('thisWeekChartCanvas') private thisWeekChartCanvas: ElementRef;
  @ViewChild('lastWeekChartCanvas') private lastWeekChartCanvas: ElementRef;
  @ViewChild('thisMonthChartCanvas') private thisMonthChartCanvas: ElementRef;

  thisWeekChart: any;
  lastWeekChart: any;
  thisMonthChart: any;

  constructor() { }

  ngOnInit() {
   
  }

  ngAfterViewInit() {
    this.loadThisWeekChart();
    this.loadLastWeekChart();
    this.loadThisMonthChart();
  }


  loadThisWeekChart() {
    this.thisWeekChart = new Chart(this.thisWeekChartCanvas.nativeElement, {
      type: 'doughnut',
      data: {
        datasets: [{
          label: '# de acertos/erros',
          data: [25, 10],
          backgroundColor: [
            '#2dd36f',
            'grey',
          ]
        }]
      },
      options: {
        responsive: true
      }
    });
  }

  loadLastWeekChart() {
    this.lastWeekChart = new Chart(this.lastWeekChartCanvas.nativeElement, {
      type: 'doughnut',
      data: {
        datasets: [{
          label: '# de acertos/erros',
          data: [12, 23],
          backgroundColor: [
            'red',
            'grey',
          ]
        }]
      },
      options: {
        responsive: true
      }
    });
  }

  loadThisMonthChart() {
    this.thisMonthChart = new Chart(this.thisMonthChartCanvas.nativeElement, {
      type: 'doughnut',
      data: {
        datasets: [{
          label: '# de acertos/erros',
          data: [25, 3],
          backgroundColor: [
            'blue',
            'grey',
          ]
        }]
      },
      options: {
        responsive: true
      }
    });
  }

}
