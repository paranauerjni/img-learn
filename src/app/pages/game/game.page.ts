import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-game',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.scss'],
})
export class GamePage implements OnInit {

  fruit = "MAÇA";

  discovered = "____";
  fruitStringDiscovered = this.discovered.split('');

  hints = ["É uma fruta", "Começa com M"];
  hintsIndex = -1;

  letters = [["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"], ["A", "S", "D", "F", "G", "H", "J", "K", "L", "Ç"], ["Z", "X", "C", "V", "B", "N", "M"]];

  constructor(private alertController: AlertController, private navController: NavController) { }

  ngOnInit() {
  }

  showHint(){
    if(this.hintsIndex == this.hints.length - 1){
      this.hintsIndex = -1;
    }
    this.hintsIndex++;

    this.presentAlert(this.hints[this.hintsIndex]);
    
  }

  tryLetter(letter){
    if(this.fruit.includes(letter)){
      this.fillDiscover(letter);
    }

    if(this.isEndGame()){
      this.presentAlertEndgame();
    }

  }

  private fillDiscover(letter){
    var aux = this.discovered.split('');

    for(var i = 0; i < this.fruit.length; i++){
      if(this.fruit[i] == letter){
        aux[i] = letter;
      }
    }

    this.discovered = aux.join('');

    this.fruitStringDiscovered = this.discovered.split('');
  }
  
  isEndGame() : Boolean{
    for(var i = 0; i < this.discovered.length; i++){
      console.log(this.discovered[i]);
      if(this.discovered[i] == "_"){
        return false;
      }
    }
    return true;
  }

  async presentAlert(message: string) {
    const alert = document.createElement('ion-alert');
    alert.header = 'Dica';
    // alert.subHeader = 'Leia com atenção!';
    alert.message = message;
    alert.buttons = ['OK'];
  
    document.body.appendChild(alert);
    await alert.present();
  }

  async presentAlertEndgame() {
    const alert = document.createElement('ion-alert');
    alert.header = 'Jogo Acabou.';
    alert.message = 'Bem Jogado!';
    alert.buttons = [
    {
        text: 'OK!',
        handler: () => {
          this.navController.navigateRoot("main");
        }
      }
    ];
  
    document.body.appendChild(alert);
    return alert.present();
  }
}
