import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Página Principal', url: '/main', icon: 'home' },
    { title: 'Adicionar Imagem', url: '/add-image', icon: 'add-circle' },
    { title: 'Perfil', url: '/profile', icon: 'person-circle' },
    { title: 'Informações IMGLearn', url: '#', icon: 'help-circle' },
  ];

  private hasLogged() {
    return localStorage.getItem("hasLogged") == "true";
  }

  private logout(){
    localStorage.setItem("hasLogged", "false");
  }

  constructor() {}
}
